﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroyer : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (!(other.CompareTag("Pipe")))        //If it collides with something (except for 'Pipe')
            Destroy(other.gameObject);      //Destroys it

        if (other.CompareTag("ColorChanger"))       //If it collides with a colorChanger
            FindObjectOfType<Spawner>().colorChangerIsDestroyed = true;     //Then there can be spawned a colorChanger
    }
}
