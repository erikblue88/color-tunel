﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    //OBSTACLES, COLORCHANGERS AND TOKENS ARE ALL USE THIS SCRIPT BECAUSE OF THEIR BEHAVIOUR IS AMLMOST THE SAME.
    //IN MY OPINION IT IS NOT NECESSARY TO BREAK THIS SCRIPT UP TO 3 SIMILAR SCRIPTS.
    //INSTEAD I USED THEIR TAGS TO IDENTIFY THEM

    public float movementSpeed, rotationSpeed;

    private GameObject player;
    private Rigidbody rb;
    private bool stoppedMoving = false;

    private Color[] colors = { Color.blue, Color.red, Color.green, Color.black };       //Here you can change the default colors for obstacles
    private Color[] colorsOfChanger = { Color.blue, Color.red, Color.green};       //Here you can change the default colors for colorChanger

	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");        //Initializes player
        rb = GetComponent<Rigidbody>();     //Initializes the rigidbody
        rb.AddForce(transform.forward * -movementSpeed);     //Makes the obstacle move towards the player
        ChangeAlpha();

        if(tag != "Token")      //If this is not a token, then selects a random color
            SelectRandomColor();
    }
	
	void Update () {
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);       //Rotates the obstacle around the Y axis
        if ((FindObjectOfType<Collision>().gameIsOver) && (!stoppedMoving))     //If the game is over and the obstacle still moves towards the player
        {
            stoppedMoving = true;
            rb.Sleep();     //Stops the obstacle
        }
	}

    public void SelectRandomColor()
    {
        switch (tag)
        {
            case "Obstacle":        //If this is a Obstacle (and not a colorChanger)
                SetColor(gameObject, colors[Random.Range(0, colors.Length)]);       //Obstacle's color will be a random color of the colors list
                break;

            case "ColorChanger":        //If this is colorChanger
                transform.Rotate(transform.right, 90f);     //Rotates it on the X axis by 90 degrees
                Color colorChangerColor;
                do     //Chooses a color until it is not identical to the player's temporary color
                {
                    GetComponent<Renderer>().material.color = colorsOfChanger[Random.Range(0, colorsOfChanger.Length)];       //Obstacle's color will be a random color of the colorsOfChanger list
                    colorChangerColor = GetColor(gameObject);
                    colorChangerColor.a = 1f;       //Sets the alpha back to 1 because colorsOfChanger colors have 0.8f alpha
                } while (colorChangerColor == GetColor(player));     //Checks if the obstacle's color is identical to the player's color
                break;
        }
    }

    public Color GetColor(GameObject obj)
    {
        return obj.GetComponent<Renderer>().material.color;     //Returns the given gameObject's color
    }

    public void SetColor(GameObject obj, Color color)
    {
        obj.GetComponent<Renderer>().material.color = color;        //Sets the given color as the given gameObject's color
    }

    public void ChangeAlpha()
    {
        for (int i = 0; i < colorsOfChanger.Length; i++)        //Changes the alpha value of each of the colors of the colorchanger's colors
            colorsOfChanger[i].a = 0.8f;
    }
}
