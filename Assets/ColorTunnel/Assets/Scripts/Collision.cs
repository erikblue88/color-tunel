﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour {

    public ParticleSystem collisionParticle, tokenParticle;

    [HideInInspector]
    public bool gameIsOver = false;


    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle"))       //If the player hit an obstacle
        {
            if (CompareColor(other.gameObject))       //If the obstacle and the player have the same color
            {
                FindObjectOfType<AudioManager>().ScoreSound();      //Plays 'scoreSound'
                FindObjectOfType<ScoreManager>().IncrementScore();     //Increments score
                Destroy(other.gameObject);      //Destroys obstalce
                SpawnCollisionParticle();       //Spawns collisionParticle
            }
            else       //If the obstacle and the player have different color
            {
                gameIsOver = true;      //Game is over
                FindObjectOfType<PipeMove>().StopPipes();       //Stops the pipes
                FindObjectOfType<AudioManager>().DeathSound();      //Plays 'deathSound'
                FindObjectOfType<GameManager>().EndPanelActivation();       //Activates EndPanel
                FindObjectOfType<Spawner>().enabled = false;        //There will be no more spawns
                StopPlayer();
            }
        }
        else if (other.CompareTag("ColorChanger"))     //If the player hit a colorChanger
        {
            FindObjectOfType<AudioManager>().ColorChangeSound();      //Plays 'colorChangerSound'
            SetColor(other.gameObject);     //Sets the player's color identical to the colorChanger's color
            FindObjectOfType<PlayerParticleController>().SetBasicParticleColor(other.gameObject);       //Sets the particle's color identical to the color of colorChanger
        }
        else if (other.CompareTag("Token"))     //If the player hit a token
        {
            FindObjectOfType<AudioManager>().TokenSound();      //Plays 'tokenSound'
            FindObjectOfType<ScoreManager>().IncrementToken();     //Increments the number of tokens
            Destroy(other.gameObject);      //Destroys token
            SpawnTokenParticle();       //Spawns tokenParticle
        }
    }

    public bool CompareColor(GameObject obj)
    {
        return GetComponent<Renderer>().material.color == obj.GetComponent<Renderer>().material.color;      //Returns true if the color of the player and the changer's color are identical, else returns false
    }

    public void SetColor(GameObject obj)
    {
        Color tempColor = obj.GetComponent<Renderer>().material.color;      //TempColor is the same color as the colorChanger's color
        tempColor.a = 1f;       //Sets the alpha to 1
        GetComponent<Renderer>().material.color = tempColor;        //Sets the player's color identical to the colorChanger's color
    }

    public void StopPlayer()
    {
        GetComponent<Renderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        FindObjectOfType<PlayerMovement>().enabled = false;
        FindObjectOfType<PlayerParticleController>().PlayDeathParticle();
    }

    public void SpawnCollisionParticle()
    {
        ParticleSystem tempParticle = Instantiate(collisionParticle, transform.position, Quaternion.identity);      //Spawns collisionParticle
        tempParticle.GetComponent<Renderer>().material.color = GetComponent<Renderer>().material.color;        //Sets the color of the collisionParticle identical to the player's color
        tempParticle.Play();        //Plays particle
        Destroy(tempParticle.gameObject, 1f);      //Destroys particle after x secs
    }

    public void SpawnTokenParticle()
    {
        ParticleSystem tokenPar = Instantiate(tokenParticle, transform.position, Quaternion.identity);       //Spawns tokenParticle
        Destroy(tokenPar.gameObject, 1f);       //Destroys tokenParticle after x seconds
    }
}
