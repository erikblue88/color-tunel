﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeMove : MonoBehaviour {

    public float movementSpeed;

    private Vector3 startPos;

	void Start () {
        startPos = transform.position;      //Gets the pipe's position when game starts
        GetComponent<Rigidbody>().AddForce(transform.forward * -movementSpeed);     //Moves pipe towards player
        Invoke("SetBackToStartPos", 1.32f);     //Pipe will be set to the starting position
	}

    public void StopPipes()
    {
        CancelInvoke("SetBackToStartPos");      //Pipes won't respawn
        GetComponent<Rigidbody>().Sleep();      //Stops pipes
    }

    public void SetBackToStartPos()
    {
        transform.position = startPos;      //Sets the pipe's position to the starting position
        Invoke("SetBackToStartPos", 1.32f);     //Invokes the same function again after x seconds
    }
}