﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticleController : MonoBehaviour {

    private ParticleSystem basicParticle, deathParticle;

	// Use this for initialization
	void Start () {
        InitializeParticles();      //Initializes particles
        basicParticle.Play();       //Plays basicParticle
	}

    public void InitializeParticles()
    {
        basicParticle = GameObject.FindGameObjectWithTag("BasicParticle").GetComponent<ParticleSystem>();       //Gets the basicParticle
        deathParticle = GameObject.FindGameObjectWithTag("DeathParticle").GetComponent<ParticleSystem>();       //Gets the deathParticle
    }

    public void SetBasicParticleColor(GameObject obj)
    {
        Color tempColor = GetColor(obj);      //TempColor is the same color as the colorChanger's color
        tempColor.a = 1f;       //Sets the alpha to 1
        SetColor(basicParticle, tempColor);        //Sets the basicParticle's color identical to the colorChanger's color
    }

    public void PlayDeathParticle()
    {
        SetColor(deathParticle, GetColor(basicParticle));       //Sets deathParticle's color identical to basicParticle's color
        basicParticle.Stop();
        deathParticle.Play();
    }

    //COLOR FUNCTIONS----------------------------------
    public Color GetColor(GameObject obj)
    {
        return obj.GetComponent<Renderer>().material.color;     //Returns the given gameObject's color
    }

    public Color GetColor(ParticleSystem obj)
    {
        return obj.GetComponent<Renderer>().material.color;     //Returns the given particleSystem's color
    }

    public void SetColor(GameObject obj, Color color)
    {
        obj.GetComponent<Renderer>().material.color = color;        //Sets the given color as the given gameObject's color
    }

    public void SetColor(ParticleSystem obj, Color color)
    {
        obj.GetComponent<Renderer>().material.color = color;        //Sets the given color as the given particleSystem's color
    }
}
