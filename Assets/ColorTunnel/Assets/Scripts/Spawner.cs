﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] obstacles;
    public GameObject colorChanger, token;
    public Material colorChangerMaterial;
    public float timeBetweenSpawns, timeReduce, minTimeBetweenSpawns;
    public int colorChangerSpawnFrequency = 3, tokenSpawnFrequency = 5;     //The lower the frequency is, the most likely to be spawned

    [HideInInspector]
    public bool colorChangerIsDestroyed = true;

	void Start () {
        Spawn();        //First spawn
	}

    public void Spawn()
    {
        if ((Random.Range(0, colorChangerSpawnFrequency) != 0) || (!colorChangerIsDestroyed))        //If it is not the time for colorChanger and there are no colorChangers
            Instantiate(obstacles[Random.Range(0, obstacles.Length)], transform.position, Quaternion.identity);       //Spawns a random obstacle to the spawner's position with the same rotation
        else      //If it is time for colorChanger
        {
            GameObject colorChangerTemp = Instantiate(colorChanger, transform.position, Quaternion.identity);       //Spawns a colorChanger to the spawner's position with the same rotation
            colorChangerTemp.GetComponent<Renderer>().material = colorChangerMaterial;      //Initializes the material
            colorChangerIsDestroyed = false;        //There is a colorChanger
        }

        if (!(FindObjectOfType<Collision>().gameIsOver))        //Invokes the next spawn only if the game is not over
        {
            Invoke("Spawn", timeBetweenSpawns);     //Next spawn after 'timeBetweenSpawns' secs
            if (Random.Range(0, tokenSpawnFrequency) == 0)      //If it is time to spawn a token
                Invoke("SpawnToken", timeBetweenSpawns / 2f);     //Then calls the function to spawn token

        }
            if ((timeBetweenSpawns - timeReduce) >= minTimeBetweenSpawns)
                timeBetweenSpawns -= timeReduce;        //reduces the timeBetweenSpawns after every spawn
    }

    public void SpawnToken()
    {
        Instantiate(token, transform.position, Quaternion.Euler(0f, 0f, Random.Range(0f, 360f)));      //Spawns token to the spawner's position with same rotation
    }
}
