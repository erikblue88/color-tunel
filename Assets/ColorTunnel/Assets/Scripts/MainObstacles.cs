﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainObstacles : MonoBehaviour {

    public int rotationSpeedMin, rotationSpeedMax;

    private int randomRot, rotationSpeed;

	void Start () {
        SetRotation();
	}
	
	void Update () {
        CheckRotation();
        CheckChildren();
    }

    public void SetRotation()
    {
        randomRot = Random.Range(0, 4);     //Generates a random number between 0 and 4 (0,1,2,3)
        rotationSpeed = Random.Range(rotationSpeedMin, rotationSpeedMax);       //Selects a random rotation speed between the min and max value
    }

    public void CheckRotation()
    {
        switch (randomRot)
        {
            case 1:     //If randomRot == 1, then it rotates right
                transform.Rotate(transform.forward, rotationSpeed * Time.deltaTime);
                break;
            case 2:     //If randomRot == 2, then it rotates left
                transform.Rotate(transform.forward, -rotationSpeed * Time.deltaTime);
                break;
        }
    }

    public void CheckChildren()
    {
        if (transform.childCount <= 1)      //If gameObject has 1 or less children
            Destroy(gameObject);        //Then it gets destroyed
    }
}
